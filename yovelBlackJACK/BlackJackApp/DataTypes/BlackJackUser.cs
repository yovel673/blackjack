﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackApp.DataTypes
{

    /// Class that inherits from the User Class - Used for creating a Blackjack Player
    class BlackJackUser : User
    {
        /// Field Variable used to store the amount of chips the player has
        private int _gameMoney;

        /// Field Variable used to store the player's score (hand value)
        private int _score;

        /// Field Variable used to store the player's soft-hand score (if they have a soft-ace)
        private int _softHandValue;

        /// Field Variable used to keep track of the amount of aces the player has drawn (in current hand)
        private int _aceCount;

        /// Constructor that initializes the base class field variables as well as the new ones
        public BlackJackUser(string name, int money, int loses, int wins, int gameMoney, int bet) : base(name, money, loses, wins)
        {
            _gameMoney = gameMoney;
            _score = 0;
            _softHandValue = 0;
            _aceCount = 0;
        }

        /// Property used to access and modify the amount of chips the player has
        public int GameMoney
        {
            get { return _gameMoney; }
            set { _gameMoney = value; }
        }

        /// Property used to access and modify the player's score
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        /// Property used to access and modify the soft-hand value of the player
        public int SoftHandValue
        {
            get { return _softHandValue; }
            set { _softHandValue = value; }
        }

        /// Property used to access and modify the ace counter of the player
        public int AceCount
        {
            get { return _aceCount; }
            set { _aceCount = value; }
        }

        /// Method used to read (load) from a file
        /// <param name="reader">reader object that reads from the file</param>
        public void Load(StreamReader reader)
        {
            //set the values of the field variables to the files contents, line by line
            _name = reader.ReadLine();
            _money = int.Parse(reader.ReadLine());
            _gameMoney = int.Parse(reader.ReadLine());
            _numWins = int.Parse(reader.ReadLine());
            _numLoses = int.Parse(reader.ReadLine());
        }

        /// Method used to write (save) to a file
        /// <param name="writer">writer object that writes to the file</param>
        public void Save(StreamWriter writer)
        {
            //write the values of the field variables to the file, line by line
            writer.WriteLine(_name);
            writer.WriteLine(_money);
            writer.WriteLine(_gameMoney);
            writer.WriteLine(_numWins);
            writer.WriteLine(_numLoses);
        }
    }
}
