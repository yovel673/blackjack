﻿using System;
using System.Diagnostics;

namespace BlackJackApp.DataTypes
{
    /// Represents the card suit with the values diamonds, hearts etc.
    public enum CardSuit
    {
        Diamonds = 1,
        Clubs,
        Hearts,
        Spades
    }

    /// Represents a card in a card game with a value and a suit.
    public class Card
    {
        /// The numeric value of the card
        private byte _value;

        /// The suit of the card
        CardSuit _suit;
        Boolean _faceUp;

        /// Constructor to create card objects given a numeric value and suit
        public Card(byte value, CardSuit suit)
        {
            _value = value;
            _suit = suit;
            _faceUp = false;

        }

        /// The numeric value of the card (Ace is 1)
        public byte Value
        {
            get { return _value; }
            set { _value = value; }
        }

        /// The card name based on its value. 1 is Ace, 11 is Jack etc.
        public string CardName
        {
            get
            {
                //determine the card name based on its value
                string cardName;
                switch (_value)
                {
                    case 1:
                        cardName = "Ace";
                        break;

                    case 11:
                        cardName = "Jack";
                        break;

                    case 12:
                        cardName = "Queen";
                        break;

                    case 13:
                        cardName = "King";
                        break;

                    default:
                        cardName = _value.ToString();
                        break;
                }

                return cardName;
            }
        }

        /// The name of the suit. This is transfered from Python which used integer
        /// to represent suit values. As we are using enums will it be necessary?
        public string SuitName
        {
            get
            {
                //determine the name of the suit
                switch (_suit)
                {
                    case CardSuit.Diamonds:
                    case CardSuit.Clubs:
                    case CardSuit.Hearts:
                    case CardSuit.Spades:
                        return _suit.ToString();

                    default:
                        Debug.Assert(false, "Unexpected suit value");
                        return "N/A";
                }
            }
        }

        public bool FaceUp
        {
            get
            {
                return _faceUp;
            }

            set
            {
                _faceUp = value;
            }
        }

        public string GetFileName()
        {
            string value = _value.ToString();
            if(value.Length == 1)
            {
                value = $"0{value}";
            }
            return $"{_suit.ToString().ToLower()[0].ToString()}{value}.png";
        }
    }
}

