﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BlackJackApp.DataTypes
{

    /// Class that inherits from the Player class - Used for creating a Blackjack Dealer
    class BlackJackDealer : Player
    {
        /// Field Variable used to store the dealer's score (hand value)
        private int _score;

        /// Field Variable used to store the dealer's soft-hand score (if they have a soft-ace)
        private int _softHandValue;

        /// Field Variable used to keep track of the amount of aces the dealer has drawn (in current hand)
        private int _aceCount;

        /// Constructor that initializes the base class field variables as well as the new ones
        public BlackJackDealer(string name) : base(name)
        {
            _score = 0;
            _softHandValue = 0;
            _aceCount = 0;
        }

        /// Property used to access and modify the dealer's score
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        /// Property used to access and modify the soft-hand value of the dealer
        public int SoftHandValue
        {
            get { return _softHandValue; }
            set { _softHandValue = value; }
        }

        /// Property used to access and modify the ace counter of the dealer
        public int AceCount
        {
            get { return _aceCount; }
            set { _aceCount = value; }
        }
    }
}
