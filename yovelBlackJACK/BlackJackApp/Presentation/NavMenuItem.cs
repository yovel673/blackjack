﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace BlackJackApp.Presentation
{
    //Class used to represent an item in the navigation menu SplitView
    public class NavMenuItem
    {
        // Property used to access and modify the label of the navigation menu item
        public string Label { get; set; }

        //Property used to access and modify the symbol of the navigation menu item
        public Symbol Symbol { get; set; }

        //Property used to access the character value of a symbol
        public char SymbolAsChar
        {
            get { return (char)Symbol; }
        }
    }
}
