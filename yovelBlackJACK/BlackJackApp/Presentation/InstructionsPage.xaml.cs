﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace BlackJackApp.Presentation
{
    // Class used with the Instructions Page - NO CODE IS NEEDED
    public sealed partial class InstructionsPage : Page
    {    
        // Creates the Page and draws it to the screen
        public InstructionsPage()
        {
            this.InitializeComponent();
        }
    }
}
