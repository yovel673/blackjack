﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BlackJackApp.Presentation;

namespace BlackJackApp
{    
    // Class used with the Main Page
    public sealed partial class MainPage : Page
    {
        // Constructor that loads the page and navigates to the instructions page by default
        public MainPage()
        {
            this.InitializeComponent();
            _frmContent.Navigate(typeof(SplashPage));
        }

        /// Event Handler that runs when the user navigates to a new page
        private void OnContentFrameNavigated(object sender, NavigationEventArgs e)
        {
            //if the page the user is navigating to is the instructions page
            if (e.SourcePageType == typeof(InstructionsPage))
            {
                //change the title of the page / change the selected item in the nav menu
                _txtPageTitle.Text = "Information";
                _lstAppNavigation.SelectedItem = _uiNavInstructions;
                _navSplitView.DisplayMode = SplitViewDisplayMode.Inline;
            }

            //if the page the user is navigating to is the blackjack page
            else if (e.SourcePageType == typeof(BlackJack))
            {
                //change the title of the page / change the selected item in the nav menu
                _txtPageTitle.Text = "Blackjack";
                _lstAppNavigation.SelectedItem = _uiNavBlackJack;
                _navSplitView.DisplayMode = SplitViewDisplayMode.Inline;
                _navSplitView.IsPaneOpen = false;
            }
        }

        // Event Handler that runs when a nav menu item is clicked
        private void OnNavigationItemClicked(object sender, ItemClickEventArgs e)
        {
            //sets variable equal to the clicked control
            NavMenuItem navMenuItem = e.ClickedItem as NavMenuItem;

            //if the control clicked is the instructions page button
            if (navMenuItem == _uiNavInstructions)
            {
                //navigate to the instructions page
                _frmContent.Navigate(typeof(InstructionsPage));
            }

            //if the control clicked is the blackjack page button
            else if (navMenuItem == _uiNavBlackJack)
            {
                //navigate to the blackjack page
                _frmContent.Navigate(typeof(BlackJack));
            }

        }
    }
}