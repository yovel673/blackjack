﻿#pragma checksum "C:\Users\Yovel-winmac\Desktop\blackjack\yovelBlackJACK\BlackJackApp\Presentation\BlackJack.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4883625F9EF10787BCD41FD3DEA0A57F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BlackJackApp.Presentation
{
    partial class BlackJack : 
        global::BlackJackApp.Presentation.CardPage, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 10.0.18362.1")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 1: // Presentation\BlackJack.xaml line 1
                {
                    global::BlackJackApp.Presentation.CardPage element1 = (global::BlackJackApp.Presentation.CardPage)(target);
                    ((global::BlackJackApp.Presentation.CardPage)element1).Loaded += this.OnLoaded;
                }
                break;
            case 2: // Presentation\BlackJack.xaml line 10
                {
                    this._grid = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 3: // Presentation\BlackJack.xaml line 27
                {
                    this._uiDealerScoreBorder = (global::Windows.UI.Xaml.Controls.Border)(target);
                }
                break;
            case 4: // Presentation\BlackJack.xaml line 51
                {
                    this._canvasDealerHand = (global::Windows.UI.Xaml.Controls.Canvas)(target);
                }
                break;
            case 5: // Presentation\BlackJack.xaml line 53
                {
                    this._uiFinalTextBorder = (global::Windows.UI.Xaml.Controls.Border)(target);
                }
                break;
            case 6: // Presentation\BlackJack.xaml line 57
                {
                    this._canvasPlayerHand = (global::Windows.UI.Xaml.Controls.Canvas)(target);
                }
                break;
            case 7: // Presentation\BlackJack.xaml line 59
                {
                    this._imgCardDeck = (global::Windows.UI.Xaml.Controls.Image)(target);
                }
                break;
            case 8: // Presentation\BlackJack.xaml line 61
                {
                    this._uiBetArea = (global::Windows.UI.Xaml.Shapes.Ellipse)(target);
                    ((global::Windows.UI.Xaml.Shapes.Ellipse)this._uiBetArea).PointerPressed += this.OnBetAreaClick;
                }
                break;
            case 9: // Presentation\BlackJack.xaml line 63
                {
                    this._txtBet = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                    ((global::Windows.UI.Xaml.Controls.TextBlock)this._txtBet).PointerPressed += this.OnBetAreaClick;
                }
                break;
            case 10: // Presentation\BlackJack.xaml line 65
                {
                    this._btnOpenPane = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnOpenPane).Click += this.OnEditUserClick;
                }
                break;
            case 11: // Presentation\BlackJack.xaml line 67
                {
                    this._uiEditProfilePane = (global::Windows.UI.Xaml.Controls.SplitView)(target);
                    ((global::Windows.UI.Xaml.Controls.SplitView)this._uiEditProfilePane).PaneClosed += this.OnPaneClosed;
                }
                break;
            case 12: // Presentation\BlackJack.xaml line 85
                {
                    this._btnDone = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnDone).Click += this.OnDoneClick;
                }
                break;
            case 13: // Presentation\BlackJack.xaml line 75
                {
                    this._lblName = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 14: // Presentation\BlackJack.xaml line 76
                {
                    this._txtNameInput = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                    ((global::Windows.UI.Xaml.Controls.TextBox)this._txtNameInput).TextChanged += this.OnNameChanged;
                }
                break;
            case 15: // Presentation\BlackJack.xaml line 77
                {
                    this._lblMoney = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 16: // Presentation\BlackJack.xaml line 78
                {
                    this._txtTotalMoney = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 17: // Presentation\BlackJack.xaml line 79
                {
                    this._btnAddFunds = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnAddFunds).Click += this.OnAddFundsClick;
                }
                break;
            case 18: // Presentation\BlackJack.xaml line 80
                {
                    this._lblChips = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 19: // Presentation\BlackJack.xaml line 81
                {
                    this._txtChipCount = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 20: // Presentation\BlackJack.xaml line 82
                {
                    this._btnAddGameMoney = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnAddGameMoney).Click += this.OnAddChips;
                }
                break;
            case 21: // Presentation\BlackJack.xaml line 72
                {
                    this._lblPaneTitle = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 22: // Presentation\BlackJack.xaml line 54
                {
                    this._txtDisplayOutcome = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 23: // Presentation\BlackJack.xaml line 32
                {
                    this._btnStart = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnStart).Click += this.OnStartClick;
                }
                break;
            case 24: // Presentation\BlackJack.xaml line 34
                {
                    this._btnHold = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnHold).Click += this.OnHoldClick;
                }
                break;
            case 25: // Presentation\BlackJack.xaml line 38
                {
                    this._btnHit = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnHit).Click += this.OnHitClick;
                }
                break;
            case 26: // Presentation\BlackJack.xaml line 39
                {
                    this._btnDouble = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this._btnDouble).Click += this.OnDoubleClick;
                }
                break;
            case 27: // Presentation\BlackJack.xaml line 41
                {
                    this.textBlock = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 28: // Presentation\BlackJack.xaml line 42
                {
                    this._uiPlayerChip = (global::Windows.UI.Xaml.Controls.ContentControl)(target);
                    ((global::Windows.UI.Xaml.Controls.ContentControl)this._uiPlayerChip).PointerPressed += this.OnPlayerChipClick;
                }
                break;
            case 29: // Presentation\BlackJack.xaml line 44
                {
                    this._uiPlayerChips = (global::Windows.UI.Xaml.Shapes.Ellipse)(target);
                }
                break;
            case 30: // Presentation\BlackJack.xaml line 45
                {
                    this._txtGameMoney = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 31: // Presentation\BlackJack.xaml line 36
                {
                    this._txtPlayerTotal = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 32: // Presentation\BlackJack.xaml line 28
                {
                    this._txtDealerTotal = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        /// <summary>
        /// GetBindingConnector(int connectionId, object target)
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 10.0.18362.1")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

